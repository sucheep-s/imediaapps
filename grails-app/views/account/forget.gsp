<!DOCTYPE html>
<html >
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <meta charset="UTF-8">
    <title></title>
    <asset:stylesheet src="login.css"/>
  </head>

  <body>

    <div class="wrapper">
	<div class="container">
		<h1>iMediaSuite</h1>
		
		<form class="form" method="POST" action="/resetPassword">
			<input type="email" name="email" placeholder="Email" autocomplete="off">
			<button type="submit" id="login-button">Reset Password</button>
			<div id="forget"><a href="/login">Back to Login</a></div>
		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>

    <!--<script src="./js/libs/jquery-2.1.4.js"></script>-->

  </body>
</html>
