<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>iMediaSuite</title>
	<asset:stylesheet src="application.css"/>

</head>

<body ng-app="myApp" ng-controller="BaseController" ng-init="init()" class="body-{{ bodyCss=='applist'?bodyCss:'account' }}">



	<div ui-view></div>

	<script src="./js/libs/jquery-2.1.4.js"></script>
	<script src="./js/libs/jquery-ui.min.js"></script>
	<script src="./js/libs/underscore-min.js"></script>
	<script src="./js/libs/angular.min.js"></script>
	<script src="./js/libs/angular-animate.min.js"></script>
	<script src="./js/libs/ui-bootstrap.min.js"></script>
	<script src="./js/libs/angular-ui-router.min.js"></script>
	<script src="./js/libs/slick-carousel.js"></script>
	<script src="./js/libs/loading-bar.js"></script>
	<script src="./js/libs/angular-slick.js"></script>
	<script src="./js/libs/angular-base64.min.js"></script>

	<!-- start template tags -->
	<script src="./js/bundle.1511061634.js"></script>
	<!-- end template tags -->
</body>

</html>
