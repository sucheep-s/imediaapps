<!DOCTYPE html>
<html ng-app="myApp" ng-controller="RegisterController" >
<head>
	<base href="/">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>iMediaSuite</title>
	<asset:stylesheet src="bootstrap.min.css"/>
	<asset:stylesheet src="register.css"/>
</head>
<body >

<div class="container">
	<nav class="navbar navbar-default register-nav">
	    <div class="navbar-header">
	        <a class="navbar-brand"><img src="./images/logo.png" width="102" height="38"></a>
	    </div>
	</nav>
	
<section>

  <div class="row">
    <div class="col-md-12">
      <div class="register-card">
        <section class="profile-header">
        	ACTIVATE USER
        	<hr>
        </section>
        <section class="profile-content">

            <div collapse="errorCollapse" class="col-sm-offset-1 alert alert-danger appMember-alert" role="alert">
              <button type="button" class="close" ng-click="errorCollapse = true" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Failed.</strong> {{msg}}
            </div>
            <div collapse="alertCollapse" class="col-sm-offset-1 alert alert-success appMember-alert" role="alert">
              <button type="button" class="close" ng-click="errorCollapse = true" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Success.</strong> Profile has been saved. Redirecting to login page.
            </div>
            <div ng-show="isSuccess" class="contact">
              <form class="form-horizontal">
                <div id="form-group-email" class="form-group">
                  <label  class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="email" id="email" class="form-control input-sm" ng-model="profile.email" readonly="" >
                  </div>
                </div>
                <div class="form-group {{hasNew}}">
                  <label class="col-sm-3 control-label">New Password<span class="req">*</span></label>
                  <div class="col-sm-8">
                    <input type="password" id="password" ng-model="profile.password" class="form-control input-sm" placeholder="New Password (at least 6 characters)">
                  </div>
                </div>
                <div class="form-group {{hasConfirm}}" id="pwd-section">
                  <label class="col-sm-3 control-label">Confirm Password<span class="req">*</span></label>
                  <div class="col-sm-8">
                    <input type="password" id="confirmPassword" ng-model="confirmPassword" class="form-control input-sm" placeholder="Confirm Password">
                  </div>
                </div>
                <div class="form-group {{hasFirstName}}" >
                  <label class="col-sm-3 control-label">First Name<span class="req">*</span></label>
                  <div class="col-sm-8">
                    <input type="text" id="firstName" class="form-control input-sm" ng-model="profile.firstName" placeholder="First Name">
                  </div>
                </div>
                <div class="form-group {{hasLastName}}">
                  <label class="col-sm-3 control-label">Last Name<span class="req">*</span></label>
                  <div class="col-sm-8">
                    <input type="text" id="lastName" class="form-control input-sm" ng-model="profile.lastName"  placeholder="Last Name">
                  </div>
                </div>
                <div class="form-group {{hasTel}}">
                  <label class="col-sm-3 control-label">Tel No.</label>
                  <div class="col-sm-8">
                    <input type="text" id="tel" class="form-control input-sm" phone-input ng-model="profile.tel" placeholder="Telephone Number">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Company</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control input-sm" ng-model="profile.company"  placeholder="Company">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Position</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control input-sm" ng-model="profile.position"  placeholder="Position">
                  </div>
                </div>
                <div class="form-group btn-zone">
                  <div class="col-sm-offset-3 col-sm-8">
                    <button type="button" ng-click="saveProfile()" ng-disabled="isDisabled"  class="btn btn-primary edit-btn">Submit</button>
                    <button type="button" ng-click="clear()" ng-disabled="isDisabled"  class="btn btn-warning edit-btn">Clear</button>
                  </div>
                </div>
              </form>
            </div>

        </section>
      </div>
    </div>
  </div>
	
</section>

</div>
<script src="./js/libs/jquery-2.1.4.js"></script>
<script src="./js/libs/angular.min.js"></script>
<script src="./js/libs/angular-animate.min.js"></script>
<script src="./js/libs/ui-bootstrap.min.js"></script>
<script src="./js/libs/angular-base64.min.js"></script>
<script src="./js/libs/loading-bar.js"></script>
<script src="./js/libs/angular-scroll.min.js"></script>

<script src="./js/register.js"></script>
</body>
</html>