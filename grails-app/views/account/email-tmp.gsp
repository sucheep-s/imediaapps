<%@ page contentType="text/html"%>
	<table  style="background: #E8E8E8; width: 750px;">
		<tr style="height: 75px;">
		</tr>
		<tr>
			<td style="padding-left: 75px;">
				<table style="background: #FFFFFF; width: 600px; border-radius: 5px;">
					<tr style="height: 40px;">
						<td style="padding-top: 45px; padding-left: 45px;">
							<img src=cid:icon-header />

						</td>
					</tr>
					<tr style="height: 35px;">
						<td>
							<hr size=1 width=520>
						</td>
					</tr>
					<tr>
						<td style="padding-left: 45px;">
							<h3>Welcome. We're glad you've joined us.</h3>
							<p style="font-size: 11px;">
								Thanks for joining i-Media digital content lifescycle suite. We thought we'd send along a few ideas <br />
								and links to help you get started.
							</p>
						</td>
					</tr>
					<tr style="height: 110px;">
						<td></td>
					</tr>
					<tr>
						<td style="padding-left: 45px;">
							<a href="${link}" style="text-decoration: none;">
								<button style="height: 50px;
												width: 180px;
												border-radius: 5px;
												background-color: #85DCF1;
												background-repeat: repeat-x;
												border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);">
									<font color="#407D8C" size="4px;"><strong>Activate Now</strong></font>
								</button>
							</a>
						</td>
					</tr>
					<tr style="height: 35px;">
						<td>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="height: 5px;">
		</tr>
		<tr>
			<td style="padding-left: 75px;">
				<table style="background: #42494F; width: 600px; border-radius: 5px;">
					<tr>
						<td style="padding-left: 20px; padding-top: 5px;">
							<p style="font-size: 10px;">
								<font color="#FFFFFF">Question? Learn more about</font>
								<font color="#000000">i-Media digital content lifescycle suite </font>
								<font color="#FFFFFF">or check out our </font>
								<font color="#000000">FAQ</font>
								<br />
								<font color="#FFFFFF" >Evaluate options. for</font>
								<font color="#000000" >teams</font>
							</p>
						</td>
						<td rowspan="2" style="padding-left: 95px;">
							<a href="https://www.facebook.com/eibizimedia" style="text-decoration: none;">
								<img src=cid:facebook />
							</a>
							<a href="http://www.youtube.com/channel/UCr19BtUvn8OYCvvYd4yt4Hg" style="text-decoration: none;">
								<img src=cid:youtube />
							</a>
						</td>
					</tr>
					<tr></tr>
				</table>
			</td>
		</tr>
		<tr style="height: 5px;">
		</tr>
		<tr>
			<td style="padding-left: 75px;">
				<table>
					<tr>
						<td>
							<img src=cid:icon-footer />
						</td>
					</tr>
					<tr>
						<td>
							<p style="font-size: 10px;">
								<font color="#000000" >Copyright &#169; 2015 i-Media digital content lifescycle suite, All rights reserved.</font>
								<br />
								<font color="#000000" >You received this email because you've expressed interest in the i-Media digital content lifescycle suite.</font>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="height: 10px;">
		</tr>
	</table>
