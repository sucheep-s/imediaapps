class UrlMappings {

	static mappings = {

		"/$action?/$id?" (controller : 'account')
        "/" (controller : 'account', action : "index")
        "500"(view:'/error')
	}
}
