package iMediaApp

import grails.converters.JSON
import grails.gsp.PageRenderer
import grails.plugins.rest.client.RestBuilder
import org.codehaus.groovy.grails.web.json.*
import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.JSON
import groovy.json.JsonSlurper
import java.security.MessageDigest

class AccountController {

	def rest = new RestBuilder()

	def emailService 
	def authUsername = "admin"
	def authPassword = "admin"

	def authString = authUsername+':'+authPassword
	def encoded = authString.bytes.encodeBase64().toString()
  	def authHeader = "Basic "+encoded

	def beforeInterceptor = [
		action: this.&checkAuthen,
		execpt: ['login', 'authen', 'register', 'activate', 'forget', 'accessdenied']
	]

	def checkAuthen() {
		if(!session.user) {
			render(view:"login")
		}
	}

	def authen() {

		def userBaseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def Email = params.email
		def Password = params.password
		try {
			def resp = rest.post(userBaseUrl + "/user/verify"){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					email = Email
					password = Password
				}
			}

			JSONObject data = new JSONObject(resp.json)
			if(data.id != 0){
				session.user = data
				redirect action: 'index'
			}
			else{
				flash.error = "Wrong username or password.";
			}
		} catch(e) {
			println e.toString();
		}
	}

    def index() {
    }
    def account() {

    }
    def login() {
    	session.user = null
    }
	def logout() {
		session.user = null
		render(view:"login")
	}
	def accessdenied(){
		render(view:"accessdenied")
	}
	def forget(){
		render(view:"forget")
	}	
	def getProfile(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def param = "/user/"+session.user.id+"/firstName,lastName,email,company,position,tel"

		try {

			def resp = rest.get(baseUrl+param){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)

			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				println "No Data"
			}

		} catch(e) {
			println e.toString()
			println "Internal server error."
		}

	}

	def updateProfile() {
		def userBaseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def param = "/user/"+session.user.id
		def FirstName = params.firstName
		def LastName = params.lastName
		def Tel = params.tel
		def Company = params.company
		def Position = params.position
		try {
			def resp = rest.put(userBaseUrl + param){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					firstName = FirstName
				  lastName = LastName
					tel = Tel
					company = Company
					position = Position
				}
			}
			JSONObject data = new JSONObject(resp.json)

			render(contentType: "text/json"){[
				'id' : data.id,
				'firstName' : data.firstName,
				'lastName' : data.lastName,
				'tel' : data.tel,
				'company' : data.company,
				'position' : data.position
			]}

		} catch(e) {
			println e.toString();
		}
	}

	def changePassword() {
		def userBaseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def param = "/user/"+session.user.id
		def CurrentPassword = params.currentPassword
		def NewPassword = params.newPassword

		try {
			String decodeBase64 = new String(CurrentPassword.decodeBase64())
			String md5 = decodeBase64.encodeAsMD5()

			def chkPwd = rest.get(userBaseUrl + param + "/password"){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(chkPwd.json)
			if(data.password != md5){
				render(contentType: "text/json"){[
					'status' : 'failed'
				]}
			}else{
				def resp = rest.put(userBaseUrl + param){
					contentType "application/json"
					header "Authorization", authHeader
					json {
						password = new String(NewPassword.decodeBase64())
					}
				}
				render(contentType: "text/json"){[
					'status' : 'success'
				]}

			}

		} catch(e) {
			println e.toString();
			render(contentType: "text/json"){[
				'status' : 'error'
			]}
		}
	}


	def getApps(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def params = "/user/applist/"+session.user.id

		try {

			def resp = rest.get(baseUrl+params){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONArray data = new JSONArray(resp.json)

			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				//flash.error = "Internal server error."
				println "No Data"
			}

		} catch(e) {
			println e.toString()
			println "Internal server error."
		}

	}

	def getUser(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def params = "/user/"+session.user.id+"/email,name,username,ownAccount"

		try{

			def resp = rest.get(baseUrl+params){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				render(contentType: "text/json"){[
					'data' : 'Internal Server Error'
				]}
			}
		} catch(e){
			println e.toString()
			println "Internal server error."
		}
	}

	def getAccountMember(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def params = "/user/accountuser/"+params.accountId

		try{

			def resp = rest.get(baseUrl+params){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONArray data = new JSONArray(resp.json)

			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				render(contentType: "text/json"){[
					'data' : 'Internal Server Error'
				]}
			}
		} catch(e){
			println e.toString()
			println "Internal server error."
		}
	}

	def getAccountApp(){
		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def Params = "/app/list/"+params.accountId

		try{

			def resp = rest.get(baseUrl+Params){ 
				contentType "application/json"
				header "Authorization", authHeader 
			} 
 
			JSONArray data = new JSONArray(resp.json)

			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				render(contentType: "text/json"){[
					'data' : 'Internal Server Error'
				]}
			} 	
  
		} catch(e){
			println e.toString()
			println "Internal server error."
		}
	}

	def getAppMember(){
		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def params = "/user/appuser/"+params.appId

		try{

			def resp = rest.get(baseUrl+params){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONArray data = new JSONArray(resp.json)

			if(data){
				render(contentType: "text/json"){[
					'data' : data
				]}
			}
			else{
				render(contentType: "text/json"){[
					'data' : []
				]}
			}
		} catch(e){
			println e.toString()
			println "Internal server error."
		}
	}

	def linkApp() {

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def IdUser = params.idUser
		def AppId = params.appId
		def Role = params.role

		try {
			def resp = rest.put(baseUrl + "/user/linkapp"){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					idUser = IdUser
					appId = AppId
					role = Role
				}
			}
			render(contentType: "text/json"){[
				'data' : 'success'
			]}

		} catch(e) {
			println e.toString();
			render(contentType: "text/json"){[
				'data' : "Internal server error."
			]}
		}
	}

	def unLinkApp() {

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def IdUser = params.idUser
		def AppId = params.appId 
		println IdUser
		println AppId
		try {
			def resp = rest.put(baseUrl + "/user/unlinkapp"){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					idUser = IdUser
					appId = AppId
				}
			}
			render(contentType: "text/json"){[
				'data' : 'success'
			]}

		} catch(e) {
			println e.toString();
			render(contentType: "text/json"){[
				'data' : "Internal server error."
			]}
		}
	}

	def inviteAccountUser() {

		String path = request.getSession().getServletContext().getRealPath("")
		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		String Email = params.email
		def AccountId = params.accountId
		def Role = params.role
		try {
			def resp = rest.post(baseUrl + "/user/invite"){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					email = Email
					accountId = AccountId
					role = Role
				}
			}

			JSONObject data = new JSONObject(resp.json) 
			if(!data.key){
				render(contentType: "text/json"){[
					'status' : 'success' 
				]}
			}
			else{
				String key = data.key
				String encoded = Email.bytes.encodeBase64().toString()
				String id = data.id 
				
				if( emailService.sendMailInvite(Email, id, encoded, key, path) ){
					render(contentType: "text/json"){[
						'status' : 'success'
					]}
				}else{
					try{
						def del = rest.delete(baseUrl+"/user/"+data.id){
							contentType "application/json"
							header "Authorization", authHeader
						}
	      		render(contentType: "text/json"){[
							'status' : 'failed'
						]}
					}catch(e){
						println e.toString();
					}
				}
			}
		}catch(e) {
			println e.toString();
		}

	}

	def register(){
		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def id = ''
		byte[] decode = params.email.decodeBase64()
		def decodeEmail = new String(decode)
		String key =  params.key
		try{
			def verify = rest.post(baseUrl + "/user/verify"){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					email = decodeEmail
					password = key 
				}
			}

			JSONObject verifyId = new JSONObject(verify.json)
			id = verifyId.id

		}catch(e){
			id = ''
		}
		if(id){

			def Param = "/user/"+id+"/status"

			try{
				def resp = rest.get(baseUrl+Param){
					contentType "application/json"
					header "Authorization", authHeader
				}
				JSONObject data = new JSONObject(resp.json)
				if(data && data.status == 'active'){
					render(view:"login")
				}
				else{
					render(view:"register")
				}
			} catch(e){
				println e.toString()
				render 'Something wrong with server. Please try again.'
			}

		}else{
			render(view:"accessdenied", model:[register:'register'])
		}

	} 

	def resetPassword(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def params = "/user/"+params.id+"/status"

		try{
			def resp = rest.get(baseUrl+params){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(data && data.status == 'active'){
				render(view:"login")
			}
			else{
				render(view:"register")
			}
		} catch(e){
			println e.toString()
			render 'Something wrong with server. Please try again.'
		}

	} 	

	def activate(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		def Id = params.id
		def FirstName = params.firstName
		def LastName = params.lastName
		def Tel = params.tel
		def Company = params.company
		def Position = params.position
		def Password = new String(params.password.decodeBase64())

		try {
			def resp = rest.put(baseUrl + "/user/" + Id){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					firstName = FirstName
					lastName = LastName
					tel = Tel
					company = Company
					position = Position
					password = Password
					status = "active"
				}
			}
			render(contentType: "text/json"){[
				'data' : 'success'
			]}

		} catch(e) {
			println e.toString();
			render(contentType: "text/json"){[
				'data' : "Internal server error."
			]}
		}

	}

	def checkAccountPermission(){
		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		try{

			def resp = rest.get(baseUrl+"/user/"+session.user.id+"/ownAccount"){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject _ownAccount = new JSONObject(resp.json)
			JSONArray ownAccount = new JSONArray(_ownAccount.ownAccount)

			def isExist =  ownAccount.find{ it.id == params.accountId.toInteger()}

			if(!isExist){
				render(contentType: "text/json"){[
					'status' : 403
				]}
			}else{
				render(contentType: "text/json"){[
					'status' : 200
				]}
			}

		}catch(e){
			println e.toString();
		}
	}

	def checkAppPermission(){

		def baseUrl = grailsApplication.config.getProperty('userBaseUrl')
		try{

			def resp = rest.get(baseUrl+"/app/list/"+params.accountId){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONArray app = new JSONArray(resp.json)
			def isExist =  app.find{ it.id == params.appId.toInteger()}

			if(!isExist){
				render(contentType: "text/json"){[
					'status' : 403
				]}
			}else{
				render(contentType: "text/json"){[
					'status' : 200
				]}
			}

		}catch(e){
			println e.toString();
		}

	}

}
