package iMediaApp


import org.codehaus.groovy.grails.web.mapping.LinkGenerator


class EmailService {
	
	LinkGenerator grailsLinkGenerator
	def sendMailInvite(String email, String id, String encoded, String key, String path){	
		String _link = grailsLinkGenerator.serverBaseURL + "/register?key=" + 	key + '&email=' + encoded + '&id=' + id
		if(email != null) {
			try{
				sendMail {
					multipart true
					to email
					from "noreply@eibiz.co.th"
					subject "i-Media digital content lifescycle suite"
					body (view:'/account/email-tmp', model : [ link : _link] )
					inline 'icon-header', 'image/png', new File(path+'/images/email/header-icon.png')
					inline 'icon-footer', 'image/png', new File(path+'/images/email/footer-icon.png')
					inline 'facebook', 'image/png', new File(path+'/images/email/facebook.png')
					inline 'youtube', 'image/png', new File(path+'/images/email/youtube.png')
				}				
				println '[[Sent Email to '+email+']]'	
				return true
			}catch(e){
				println e
				return false
			}
		}else {
			return false
		}
	}

}
