module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: ["dist", 'tmp', 'web-app/js/*.js'],

    jshint: {
      all: [ 'Gruntfile.js', 'app/*.js', 'app/**/*.js' ]
    },

    html2js: {
      options: {
        base: 'app',
        module: 'myApp.templates',
        singleModule: true,
        useStrict: true,
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        }
      },
      dist: {
        src: [ 'app/views/*.html' ],
        dest: 'tmp/templates.js'
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [ 'app/*.js','app/services/*.js','app/controllers/*.js', 'app/directives/*.js', 'tmp/*.js'],
        dest: 'dist/bundle.js'
      },
      register: {
        src: ['app/register/*.js'],
        dest: 'dist/register.js'
      }
    },
    uglify: {
      dist: {
        files: {
          'web-app/js/bundle.<%= grunt.template.today("yymmddHHMM")%>.js': [ 'dist/bundle.js' ],
          'web-app/js/register.js': ['dist/register.js']
        },
        options: {
          mangle: false
        }
      }
    },
    tags: {
        buildScripts: {
            options: {
                scriptTemplate: '<script src="./js/bundle.<%= grunt.template.today("yymmddHHMM")%>.js"></script>',
                openTag: '<!-- start template tags -->',
                closeTag: '<!-- end template tags -->'
            },
            src: [
                'web-app/js/bundle.<%= grunt.template.today("yymmddHHMM")%>.js'
            ],
            dest: 'grails-app/views/account/index.gsp'
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-html2js');
  grunt.loadNpmTasks('grunt-script-link-tags');
  // Loading of tasks and registering tasks will be written here
  grunt.registerTask('default', [ 'clean', 'jshint', 'html2js:dist', 'concat:dist', 'concat:register', 'uglify:dist', 'tags']);

};
