myApp.controller('AccountController', function($scope, $state, $stateParams, $timeout, AccountService, UserService, $rootScope){
	$scope.currState = $state;
    $scope.accounts = [];
    $scope.active = '';
    $scope.accountId = '';

    $scope.$watch('accountId', function(n, o){

        if(typeof n != 'undefined' && n !== ''){

            $scope.active = n;
        }

    });

    $scope.init = function(){

        /*$rootScope.$on('accounts', function (event, data) {
            $scope.accounts = data;
        });*/

        UserService.getUser().success(function(data){
            $scope.accounts = JSON.parse(data.data.ownAccount);
        });

    };

    $scope.$watch('currState.current.name', function(newValue, oldValue) {

    	if(newValue == 'app.account.detail.memberAccount'){
    		$scope.activeMenu = 'member';
    	}
        else if(newValue == 'app.account'){
            $scope.activeMenu = newValue;
        }
        else{
    		$scope.activeMenu = 'app';
    	}
    });

});



