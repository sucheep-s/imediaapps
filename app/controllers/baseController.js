myApp.controller('BaseController', function($scope, $state, $window, UserService, $rootScope){

	$scope.currState = $state;

    $scope.$watch('currState.current.name', function(newValue, oldValue) {
    	$scope.bodyCss = newValue;
    });

	$scope.init = function(){
		getUser();
	};

	function getUser(){
		UserService.getUser().success(function(data){

			var user = data.data;
			var accountLength = JSON.parse(user.ownAccount).length;

			$scope.accountLength = accountLength;
			$scope.user = user;

			$rootScope.$broadcast('accounts', JSON.parse(user.ownAccount));

			if(accountLength === 0){
				$scope.accountOwner = false;
			}
			else{
				$scope.accountOwner = true;
			}

		});
	}
	$scope.logout = function () {
		UserService.logout();
		$window.location.href = '/login';
	};

});
