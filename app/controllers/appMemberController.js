myApp.controller('AppMemberController', function($scope, $rootScope, $stateParams, $window, $timeout, AccountService){

  $scope.chooseRole = false;
  $scope.btnCollapse = true;
  $scope.saveSuccess = false;
  $scope.stateDelete = false;
  var _accountId = $stateParams.accountId;
  var _appId = $stateParams.appId;
  var _appMember = [];
  var _accountMember = [];
  $scope.appMembers = [];
  $scope.accountMembers = [];
  $scope.successCollapse = true;
  $scope.existCollapse = true;
  $scope.options = ['Administrator', 'Designer', 'Approver', 'Reporter'];
  $scope.newRole = $scope.options[0];

	$scope.init = function(){
		checkAppPermission();
	};

  function checkAppPermission(){

    AccountService.checkAppPermission(_accountId, _appId).success(function (data){
      console.log(data);
      if(data.status === 403){
        $window.location.href = '/accessdenied';
      }else{
        getAccountMember();
        getAppMember();
      }
    });

  }

  function getAccountMember(){
    $scope.accMemberLoading = true;
		AccountService.getAccountMember(_accountId).success(function(data){
			$scope.accountMembers = data.data;
		}).finally(function(){
      $scope.accMemberLoading = false;
    });
	}

	function getAppMember(){
    $scope.appMemberLoading = true;
		AccountService.getAppMember(_appId).success(function(data){
      _.map(data.data, function(item){
        if(item.relation.role){
          _appMember.push({
            id : item.user.id,
            email : item.user.email,
            role : item.relation.role,
            _role : item.relation.role
          });
        }
      });
			$scope.appMembers = _appMember;
		}).finally(function(){
      $scope.appMemberLoading = false;
    });
	}
  $scope.assignNewRole = function(r){
    $scope.newRole = r;
  };
  $scope.addAppMember = function(accMember){

    if( !_.findWhere($scope.appMembers, { id : accMember.id}) ){
      var obj = {
        id : accMember.id,
        email : accMember.email,
        role : $scope.newRole,
        _role : $scope.newRole,
      };
      $scope.saveMemberRole(obj); 
      $scope.appMembers.push(obj);
    }else{
      $scope.existCollapse = false;
    }
  };

  $scope.removeAppMember = function(obj){
    var newObj = {
      idUser : obj.id,
      appId : parseInt(_appId),
    };
    AccountService.unLinkApp(newObj).success(function(data){
        $scope.successCollapse = false;
        var index = $scope.appMembers.indexOf(obj);
        $scope.appMembers.splice(index, 1);
    });
    
  };

  $scope.saveMemberRole = function(obj){
    var newObj = {
      idUser : obj.id,
      appId : parseInt(_appId),
      role : obj.role
    };
    AccountService.linkApp(newObj).success(function(data){
        $scope.successCollapse = false;
    });
  };

  $scope.$watch('successCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.successCollapse = true;
      }, 2000);
    }
  });
  $scope.$watch('existCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.existCollapse = true;
      }, 2000);
    }
  });
  

});
