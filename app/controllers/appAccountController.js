myApp.controller('AppAccountController', function($scope, $stateParams, $window, AccountService){
	$scope.inviteAppCollapsed = true;
	var accountId = $stateParams.accountId;
	$scope.init = function(){
		checkAccountPermission();
	};

	function checkAccountPermission() {

		AccountService.checkAccountPermission(accountId).success(function (data){
			if(data.status === 403){
				$window.location.href = '/accessdenied';
			}else{
				getAccountApp();
			}
		});

	}

	function getAccountApp(){
		$scope.appListLoading = true;
		AccountService.getAccountApp(accountId).success(function (data){

			$scope.accountApps = data.data;
			
		}).finally(function(){
			$scope.appListLoading = false;
		});
	}

});
