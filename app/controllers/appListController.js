myApp.controller('AppListController', function($scope, $window, UserService){

	$scope.init = function(){
		getApps();
	};

	function getApps(){
		UserService.getApps().success(function(data){

			var apps = [];
			_.map(data.data, function(app){
				apps.push({
					name : app.name,
					url : app.url					
				});
			});

			if(apps.length >= 4){
				$scope.itemClass = 'items';
			}else{
				switch (apps.length){
					case 3:
						$scope.itemClass = 'threeItems';
						break;
					case 2:
						$scope.itemClass = 'twoItems';
						break;
					case 1:
						$scope.itemClass = 'oneItem';
						break;
				}
			}

			$scope.apps = apps;
		});
	}

	$scope.openApp = function(url){
		$window.open('http://'+url, '_blank');
	};

});
