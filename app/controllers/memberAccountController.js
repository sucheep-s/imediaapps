myApp.controller('MemberAccountController', function($scope, $stateParams, $window, $timeout, AccountService, focus){
	$scope.inviteCollapsed = true;
	var accountId = $stateParams.accountId;
	$scope.isDisabled = false;
	$scope.accountMembers = [];
	$scope.email = '';
	$scope.options = ['user', 'owner'];
	$scope.role = $scope.options[0];
	$scope.msg = '';
	$scope.hasEmail = '';
	$scope.alertCollapse = true;
	$scope.errorCollapse = true;
	$scope.isDisabled = false;
	
	$scope.init = function(){
		checkAccountPermission();
	};

	function checkAccountPermission() {

		AccountService.checkAccountPermission(accountId).success(function (data){
			if(data.status === 403){
				$window.location.href = '/accessdenied';
			}else{
				getAccountMember();
			}
		});

	}

	function getAccountMember(){
		$scope.memberListLoading = true;
		AccountService.getAccountMember(accountId).success(function(data){
			$scope.accountMembers = data.data;
		}).finally(function(){
			$scope.memberListLoading = false;
		});
	} 

	$scope.inviteUser = function(){

		if(!$scope.email){
			$scope.hasEmail = 'has-error';
			focus('email');
		}else{
			$scope.hasEmail = '';
			$scope.isDisabled = true;
			var obj = {
				 accountId : $stateParams.accountId,
				 email : $scope.email,
				 role : $scope.role,
				 status : 'new'
			};

			var isExist = _.findWhere($scope.accountMembers, {email : obj.email});
			if(isExist){
				$scope.msg = 'This Email is already in account.';
				$scope.errorCollapse = false;
				$scope.isDisabled = false;
			}else{

				AccountService.inviteAccountUser(obj).success(function(data){
					if(data.status === 'success'){
						$scope.msg = 'User has been invited.';
						$scope.alertCollapse = false;
						$scope.accountMembers.push(obj);
					}else{
						$scope.msg = 'There is something wrong with server. Please try again.';
						$scope.errorCollapse = false;				
					}
				}).error(function(){
					$scope.msg = 'Internal Server Error.';
					$scope.errorCollapse = false;

				}).finally(function(){
					$scope.email = '';
					$scope.isDisabled = false;
					$scope.inviteCollapsed = true;

				});

			}

		}
	};

  $scope.$watch('alertCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.alertCollapse = true;
      }, 3000);
    }
  });

  $scope.$watch('errorCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.errorCollapse = true;
      }, 3000);
    }
  });  

});
