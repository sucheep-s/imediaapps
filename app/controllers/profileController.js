myApp.controller('ProfileController', function($scope, $stateParams, $timeout, ProfileService, focus, $base64){

  $scope.state = 'contact';
  $scope.isContact = true;
  $scope.isSetting = false;
  $scope.hasFirstName = '';
  $scope.hasLastName = '';
  $scope.hasTel = '';
  $scope.hasCurrent = '';
  $scope.hasNew = '';
  $scope.hasConfirm = '';
  $scope.msg = '';
  $scope.alertCollapse = true;
  $scope.errorCollapse = true;
  $scope.isDisabled = false;

  $scope.profile = {};
  $scope.password = {};
  $scope._profile = {};

  $scope.init = function(){
    getProfile();
  };

  function getProfile(){
    ProfileService.getProfile().success(function(data){
      $scope.profile = data.data;
      angular.copy($scope.profile, $scope._profile);
    });
  }

  $scope.checkState = function(state){
    if(state === 'contact'){
      $scope.state = 'contact';
      $scope.isContact = true;
      $scope.isSetting = false;
    }else{
      $scope.state = 'setting';
      $scope.isContact = false;
      $scope.isSetting = true;
    }
  };

  function saveProfile(){
    $scope.isDisabled = true;
    $scope.hasFirstName = '';
    $scope.hasLastName = '';
    $scope.hasTel = '';
    var obj = {
      firstName : $scope.profile.firstName,
      lastName : $scope.profile.lastName,
      tel : $scope.profile.tel,
      company : $scope.profile.company,
      position : $scope.profile.position
    };
    ProfileService.updateProfile(obj).success(function(data){
      $scope.msg = 'Profile has been updated.';
      $scope.alertCollapse = false;
    }).finally(function(){
      $scope.isDisabled = false;
    });
  }

  $scope.saveProfile = function(){
    if($scope.profile.firstName === ''){
      $scope.hasFirstName = 'has-error';
      focus('firstName');
    }else if($scope.profile.lastName === ''){
      $scope.hasFirstName = '';
      $scope.hasLastName = 'has-error';
      focus('lastName');
    }else if($scope.profile.tel.length !== 0){

      if($scope.profile.tel.length !== 10){
        $scope.hasTel = 'has-error';
      }else{
        saveProfile();
      }
    }else{
      saveProfile();
    }
  };

  $scope.resetProfile = function(){
    angular.copy($scope._profile, $scope.profile);
  };

  $scope.changePassword = function(){
    if(!$scope.password.current){
      $scope.hasCurrent = 'has-error';
    }else if(!$scope.password.new || $scope.password.new.length < 6){
      $scope.hasCurrent = '';
      $scope.hasNew =  'has-error';
    }else if(!$scope.password.confirm){
      $scope.hasCurrent = '';
      $scope.hasNew =  '';
      $scope.hasConfirm = 'has-error';
    }else if($scope.password.new !== $scope.password.confirm){
      $scope.hasCurrent = '';
      $scope.hasNew =  '';
      $scope.hasConfirm = '';

      $scope.errorCollapse = false;
      $scope.msg = 'Password is not matched.';
    }else{
      $scope.isDisabled = true;
      $scope.hasCurrent = '';
      $scope.hasNew =  '';
      $scope.hasConfirm = '';

      var obj = {
        currentPassword : $base64.encode($scope.password.current),
        newPassword : $base64.encode($scope.password.new)
      };
      ProfileService.changePassword(obj).success(function(data){
        if(data.status === 'success'){
          $scope.password = {};
          $scope.msg = 'Password has been updated.';
          $scope.alertCollapse = false;
        }else{
          $scope.msg = 'Current Password is not correct.';
          $scope.errorCollapse = false;
        }
      }).error(function(){

          $scope.msg = 'Internal Server Error.';
          $scope.errorCollapse = false;

      }).finally(function(){

        $scope.isDisabled = false;
        
      });
    }
  };

  $scope.resetPassword = function(){
    $scope.password = {};
  };

  $scope.$watch('alertCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.alertCollapse = true;
      }, 3000);
    }
  });
  $scope.$watch('errorCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.errorCollapse = true;
      }, 2000);
    }
  });

});
