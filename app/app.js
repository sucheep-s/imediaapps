var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
  return $window._; // assumes underscore has already been loaded on the page
}]);
var compress = angular.module('removeSpace', []).filter('removeSpace', function() {
  return function(string) {
      if (!angular.isString(string)) {
          return string;
      }
      return string.replace(/[\s]/g, '');
  };
});

var myApp = angular.module('myApp', ['ui.router', 'myApp.templates', 'angular-loading-bar', 'ngAnimate', 'ui.bootstrap', 'slick', 'underscore', 'removeSpace', 'base64']);

myApp.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams, $templateCache) {

    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (typeof(current) !== 'undefined'){
            $templateCache.remove(current.templateUrl);
        }
    });

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}]);

myApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider){


    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.common = {};
    }
    $httpProvider.defaults.headers.common["Cache-Control"] = "no-cache";
    $httpProvider.defaults.headers.common.Pragma = "no-cache";
    $httpProvider.defaults.headers.common["If-Modified-Since"] = "0";


	$urlRouterProvider.otherwise("/applist");
	$urlRouterProvider.when('/app/account/:accountId', '/app/account/:accountId/applist');

    $stateProvider
    .state('applist', {
    	url: "/applist",
        controller: 'AppListController',
        templateUrl: 'views/applist.html'
    })
    .state('app', {
        url: "/app",
        controller: 'AccountController',
        templateUrl: 'views/app.html'
    })
    .state('app.myprofile', {
        url: "/profile",
        controller: 'ProfileController',
        templateUrl: 'views/myprofile.html'
    })
    .state('app.account', {
    	url: "/account",
        controller: 'AccountController',
        templateUrl: 'views/account.html'
    })
    .state('app.account.detail', {
    	url: "/:accountId",
    	controller: 'AccountController',
        templateUrl: 'views/account.detail.html'
    })
    .state('app.account.detail.appAccount', {
    	url: "/applist",
        controller: 'AppAccountController',
        templateUrl: 'views/account.detail.appAccount.html',
    })
    .state('app.account.detail.memberAccount', {
    	url: "/memberlist",
        controller: 'MemberAccountController',
        templateUrl: 'views/account.detail.memberAccount.html'
    })
    .state('app.account.detail.appMember', {
        url: "/app/:appId",
        controller: 'AppMemberController',
        templateUrl: 'views/account.detail.appMember.html'
    });

}]);
