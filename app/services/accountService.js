myApp.factory('AccountService', function($http){

  var factory = {};

  factory.getAccountMember = function(accountId){
      return $http.get('/getAccountMember', {
        ignoreLoadingBar: true,
        params:{"accountId": accountId}
      });
  };

  factory.getAccountApp = function(accountId){
      return $http.get('/getAccountApp', {
        ignoreLoadingBar: true,
        params:{"accountId": accountId}
      });
  };

  factory.getAppMember = function(appId){
      return $http.get('/getAppMember', {
        ignoreLoadingBar: true,
        params:{"appId": appId}
      });
  };

  factory.linkApp = function(obj){

      return $http.post('/linkApp', $.param({'idUser': obj.idUser, 'appId': obj.appId, 'role': obj.role}),
      {
          headers:
          {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
      });
  }; 

  factory.unLinkApp = function(obj){

      return $http.post('/unLinkApp', $.param({'idUser': obj.idUser, 'appId': obj.appId}),
      {
          headers:
          {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
      });
  };

  factory.checkAccountPermission = function(accountId){
      return $http.get('/checkAccountPermission', {
        ignoreLoadingBar: true,
        params:{"accountId": accountId}
      });      
  };

  factory.checkAppPermission = function(accountId, appId){
      return $http.get('/checkAppPermission', {
        ignoreLoadingBar: true,
        params:{"accountId": accountId, "appId": appId}
      });      
  };  

  factory.inviteAccountUser = function(obj){

      return $http.post('/inviteAccountUser', $.param(obj),
      {
          headers:
          {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
      });
  };



  return factory;

});
