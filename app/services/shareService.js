myApp.service('ShareService', function() {

  var accountMemberLength = 0;

  var setAccountMemberLength = function(num){
    accountMemberLength = num;
  };
  var getAccountMemberLength = function(){
    return accountMemberLength;
  };

  return {
    setAccountMemberLength: setAccountMemberLength,
    getAccountMemberLength: getAccountMemberLength
  };

});
