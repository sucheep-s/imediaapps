myApp.factory('UserService', function($http){

	var factory = {};

	factory.getApps = function(){
		return $http.get('/getApps', {
			ignoreLoadingBar: true,
		});
	};

	factory.getUser = function(){
		return $http.get('/getUser', {
			ignoreLoadingBar: true,
		});
	};
	factory.logout = function(){
		return $http.get('/logout', {
			ignoreLoadingBar: true,
		});
	};

	return factory;

});
