myApp.factory('ProfileService', function($http){

  var factory = {};

  factory.getProfile = function(){
      return $http.get('/getProfile');
  };

  factory.updateProfile = function(obj){

    return $http.post('/updateProfile', $.param(obj),
    {
        headers:
        {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    });

  };

  factory.changePassword = function(obj){

    return $http.post('/changePassword', $.param(obj),
    {
        headers:
        {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    });

  };

  return factory;

});
