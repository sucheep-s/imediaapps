var myApp = angular.module('myApp', ['ngAnimate', 'ui.bootstrap', 'angular-loading-bar', 'base64', 'duScroll'], function($locationProvider) {
  $locationProvider.html5Mode(true);
});

myApp.factory('focus', function($timeout, $window) {
  return function(id) {
    // timeout makes sure that is invoked after any other event has been triggered.
    // e.g. click events that need to run before the focus or
    // inputs elements that are in a disabled state but are enabled when those events
    // are triggered.
    $timeout(function() {
      var element = $window.document.getElementById(id);
      if(element)
        element.focus();
    });
  };
});

myApp.directive('eventFocus', function(focus) {
  return function(scope, elem, attr) {
    elem.on(attr.eventFocus, function() {
      focus(attr.eventFocusId);
    });

    // Removes bound events in the element itself
    // when the scope is destroyed
    scope.$on('$destroy', function() {
      element.off(attr.eventFocus);
    });
  };
});


myApp.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});

myApp.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});

myApp.controller('RegisterController', function($scope, $timeout, $location, $base64, $document,  $window, focus, RegisterService){
  var email = $location.search().email;
  var key = $location.search().key;
  var _id = $location.search().id;

  $scope.hasFirstName = '';
  $scope.hasLastName = '';
  $scope.hasTel = '';
  $scope.hasNew = '';
  $scope.hasConfirm = '';
  $scope.isSuccess = true;
  $scope.msg = '';
  $scope.confirmPassword = '';
  $scope.profile = {};
  $scope.profile.email = $base64.decode(email);
  $scope.errorCollapse = true;
  $scope.alertCollapse = true;
  $scope.isDisabled = false;

  function saveProfile(){
    $scope.isDisabled = true;
    $scope.hasFirstName = '';
    $scope.hasLastName = '';
    $scope.hasTel = '';
    $scope.hasNew =  '';
    $scope.hasConfirm = '';
    var obj = {
      id : _id,
      firstName : $scope.profile.firstName,
      lastName : $scope.profile.lastName,
      tel : $scope.profile.tel,
      company : $scope.profile.company,
      position : $scope.profile.position,
      password : $base64.encode($scope.profile.password)
    };
    RegisterService.register(obj).success(function(data){
      var top = 50;
      var duration = 1500; //milliseconds
      $document.scrollTop(top, duration);
      $scope.alertCollapse = false;
    }).error(function(){
      $scope.errorCollapse = false;
      $scope.msg = 'There is something wrong with server. Please try again.';
    }).finally(function(){
      $timeout(function () {
        $window.location.assign('/login');
      }, 3000);
    });
  }

  function saveSuccess(){

  }

  $scope.saveProfile = function(){

    if(!$scope.profile.password || $scope.profile.password.length < 6){
      focus('password');
      $scope.hasNew =  'has-error';
    }else if(!$scope.confirmPassword){
      focus('confirmPassword');
      $scope.hasNew =  '';
      $scope.hasConfirm = 'has-error';
    }else if($scope.profile.password !== $scope.confirmPassword){
      $scope.hasNew =  '';
      $scope.hasConfirm = '';
      $scope.errorCollapse = false;
      $scope.msg = 'Password is not matched.';
    }else if(!$scope.profile.firstName){
      focus('firstName');
      $scope.hasNew =  '';
      $scope.hasConfirm = '';
      $scope.hasFirstName = 'has-error';
      focus('firstName');
    }else if(!$scope.profile.lastName){
      focus('lastName');
      $scope.hasFirstName = '';
      $scope.hasNew =  '';
      $scope.hasConfirm = '';  
      $scope.hasLastName = 'has-error';
      focus('lastName');
    }else if($scope.profile.tel){
      $scope.hasFirstName = '';
      $scope.hasNew =  '';
      $scope.hasConfirm = '';  
      $scope.hasLastName = '';      
      if($scope.profile.tel.length !== 10){
        $scope.hasTel = 'has-error';
      }else{
        saveProfile();
      }
    }else{
      saveProfile();
    }

  };

  $scope.clear = function(){
    $scope.profile = {};
    $scope.profile.email = email;
  };

  $scope.$watch('errorCollapse', function(newValue, oldValue){
    if(newValue === false){
      $timeout(function () {
        $scope.errorCollapse = true;
      }, 2500);
    }
  });

});


myApp.factory('RegisterService', function($http){

  var factory = {};

  factory.register = function(obj){

    return $http.post('/activate', $.param(obj),
    {
        headers:
        {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    });

  };

  return factory;

});
